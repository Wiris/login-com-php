<?php
include "./conexao.php";
if (isset($_POST["email"]) || isset($_POST["password"])) {
    if (strlen($_POST["email"]) == 0) {
        echo "Preencha seu email:";
    } else if (strlen($_POST["password"]) == 0) {
        echo "Preencha sua Senha:";
    } else {
        $email = $mysqli->real_escape_string($_POST["email"]);
        $senha = $mysqli->real_escape_string($_POST["password"]);

        $sql = "SELECT * FROM usuario WHERE email = '$email' AND senha = '$senha'";
        $sql_query = $mysqli->query($sql) or die("Falha na Execução da consulta!" . $mysqli->error);

        $quantidade = $sql_query->num_rows;
        if ($quantidade == 1) {

            $usuario = $sql_query->fetch_assoc();
            if (!isset($_SESSION)) {
                session_start();
            }
            $_SESSION['user'] = $usuario['id'];
            $_SESSION['name'] = $usuario['nome'];

            header("Location: painel.php");
        } else {
            echo "Falha ao logar! Email ou Senha Incorretos!";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <form action="" method="post">
        <h1>Acesse sua Conta</h1>
        <p>
            <label for="email">Email</label>
            <input type="email" name="email" id="email">
        </p>
        <p>
            <label for="password">Senha</label>
            <input type="password" name="password" id="password">
        </p>
        <p>
            <input type="submit" value="Entrar">
        </p>
    </form>
</body>
</html>
